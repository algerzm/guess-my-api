import { Body, Controller, Post, UnauthorizedException } from '@nestjs/common'
import { AuthService } from './auth.service'
import { loginData } from 'src/interfaces/auth/loginData.interface'
import { User } from 'src/schemas/user.schema'

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async login(@Body() loginDataBody: loginData): Promise<any> {
    const { email, password } = loginDataBody
    const user: User = await this.authService.validateUser(email, password)

    if (!user) {
      throw new UnauthorizedException()
    }

    return await this.authService.login(user)
  }
}
