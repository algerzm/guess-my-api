import { Injectable } from '@nestjs/common'
import { UsersService } from 'src/users/users.service'
import { JwtService } from '@nestjs/jwt'
import { jwtPayload } from 'src/interfaces/auth/jwtPayload.interface'
import * as bcrypt from 'bcrypt'
import { User } from 'src/schemas/user.schema'

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService, private jwtService: JwtService) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user: User = await this.usersService.getUserByEmail(email)

    if (user) {
      let isPasswordCorrect = await bcrypt.compare(pass, user.password)
      if (isPasswordCorrect) {
        const { password, ...result } = user
        return result
      }
    }

    return null
  }

  async login(user: any) {
    const payload: jwtPayload = { email: user.email, userId: user._id }
    return {
      access_token: this.jwtService.sign(payload),
    }
  }
}
