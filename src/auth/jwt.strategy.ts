import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { jwtPayload } from 'src/interfaces/auth/jwtPayload.interface'

@Injectable()
export class JwtSrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    })
  }

  async validate(payload: jwtPayload): Promise<any> {
    return { email: payload.email, userId: payload.userId }
  }
}
