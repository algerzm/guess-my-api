import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule } from '@nestjs/config'

import { AuthModule } from './auth/auth.module'
import { BookModule } from './book/book.module'
import { UsersModule } from './users/users.module'

const connectionString = 'mongodb://guessmyapi:guessmy0214@74.208.37.21:27017/guessmy'

@Module({
  imports: [ConfigModule.forRoot(), UsersModule, MongooseModule.forRoot(connectionString), AuthModule, BookModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
