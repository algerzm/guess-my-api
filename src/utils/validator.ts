import { userDataValidation } from 'src/interfaces/validation/userDataValidation.interface'
import { errorMessages } from 'src/interfaces/validation/errorMessages.interface'
export class Validator {
  validatorMessages = {
    email: 'Email is not valid.',
    password: 'Password is not valid.',
    emailExists: 'Email already exists.',
    userName: 'UserName already exists.',
  }

  isEmailInvalid(email): Boolean {
    const em =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return em.test(String(email).toLowerCase()) ? false : true
  }
  isPasswordInvalid(password): Boolean {
    const passt = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
    return passt.test(String(password)) ? false : true
  }

  isUserDataInvalid(email, password): userDataValidation {
    let invalidData = {
      email: this.isEmailInvalid(email),
      password: this.isPasswordInvalid(password),
    }

    return invalidData
  }

  chooseErrorMessage(email, password): errorMessages {
    let { email: emailError, password: passwordError } = this.isUserDataInvalid(email, password)

    let errorMessages: errorMessages = {
      error: false,
      message: '',
    }

    if (emailError) {
      errorMessages = {
        error: true,
        message: this.validatorMessages.email,
      }
    } else if (passwordError) {
      errorMessages = {
        error: true,
        message: this.validatorMessages.password,
      }
    }

    return errorMessages
  }
}
