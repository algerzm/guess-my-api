import { HttpResponse } from './HttpResponse.class'

export class SuccessResponse extends HttpResponse {
  private data: any

  constructor(status, data) {
    super(status)
    this.data = data
  }

  public setData(data: any) {
    this.data = data
  }

  public getData() {
    return {
      error: false,
      data: this.data,
    }
  }
}
