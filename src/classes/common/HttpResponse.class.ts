export abstract class HttpResponse {
  private status: number
  abstract getData(): any

  constructor(status) {
    this.status = status
  }

  public setStatus(status: number) {
    this.status = status
  }

  public getStatus() {
    return this.status
  }
}
