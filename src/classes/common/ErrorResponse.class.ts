import { HttpResponse } from './HttpResponse.class'

export class ErrorResponse extends HttpResponse {
  private message: string
  private logMessage: string

  constructor(status, message, logMessage) {
    super(status)
    this.message = message
    this.logMessage = logMessage
  }

  public setMessage(message: string) {
    this.message = message
    return this
  }

  public getMessage() {
    return this.message
  }

  public setLogMessage(logMessage: string) {
    this.logMessage = logMessage
  }

  public getLogMessage() {
    return this.logMessage
  }

  public getData() {
    return {
      error: true,
      message: this.message,
    }
  }
}
