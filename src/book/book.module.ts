import { Module } from '@nestjs/common'
import { BookController } from './book.controller'
import { BookService } from './book.service'
import { UsersModule } from 'src/users/users.module'
import { MongooseModule } from '@nestjs/mongoose'
import { BookAnswerHistory } from 'src/schemas/bookAnswersHistory'
import { BookAnswerHistorySchema } from 'src/schemas/bookAnswersHistory'

@Module({
  imports: [UsersModule, MongooseModule.forFeature([{ name: BookAnswerHistory.name, schema: BookAnswerHistorySchema }])],
  controllers: [BookController],
  providers: [BookService],
  exports: [BookService],
})
export class BookModule {}
