import { Injectable } from '@nestjs/common'
import { UsersService } from 'src/users/users.service'
import { Model } from 'mongoose'
import { v4 as uuidv4 } from 'uuid'
import { BookItem } from 'src/schemas/bookItem.schema'
import { ErrorResponse } from 'src/classes/common/ErrorResponse.class'
import { HttpStatus } from '@nestjs/common'
import { SuccessResponse } from 'src/classes/common/SuccessResponse.class'
import { BookAnswerHistory } from 'src/schemas/bookAnswersHistory'
import { BookAnswerHistoryDocument } from 'src/schemas/bookAnswersHistory'
import { InjectModel } from '@nestjs/mongoose'

@Injectable()
export class BookService {
  constructor(
    private readonly usersService: UsersService,
    @InjectModel(BookAnswerHistory.name) private readonly bookAnswersModel: Model<BookAnswerHistoryDocument>
  ) {}

  async addBookItem(userId: number, bookData: BookItem) {
    try {
      let { user } = await this.usersService.getUserById(userId)

      if (!user) {
        const errorMessage = 'User was not found.'
        const logMessage = 'User ID not found in data base'
        return new ErrorResponse(HttpStatus.NOT_FOUND, errorMessage, logMessage)
      }

      const itemId = uuidv4()
      bookData.id = itemId

      user.bookList = [...user.bookList, bookData]
      const res = await this.usersService.updateUser(userId, user)
      return new SuccessResponse(HttpStatus.ACCEPTED, res)
    } catch (err) {
      const errorMessage = 'Problem ocurred while saving in database'
      return new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage, err)
    }
  }

  async modifyBookItem(userId: number, bookData: BookItem) {
    try {
      let { user } = await this.usersService.getUserById(userId)

      if (!user) {
        const errorMessage = 'User was not found.'
        const logMessage = 'User ID not found in data base'
        return new ErrorResponse(HttpStatus.NOT_FOUND, errorMessage, logMessage)
      }

      let bookListWithoutOldItem = user.bookList.filter((item) => item.id !== bookData.id)
      let newBookList = [...bookListWithoutOldItem, bookData]

      user.bookList = newBookList
      const res = await this.usersService.updateUser(userId, user)
      return new SuccessResponse(HttpStatus.ACCEPTED, res)
    } catch (err) {
      const errorMessage = 'Problem ocurred while saving in database'
      return new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage, err)
    }
  }

  async removeBookItem(userId: number, bookItemId: string) {
    try {
      let { user } = await this.usersService.getUserById(userId)

      if (!user) {
        const errorMessage = 'User was not found.'
        const logMessage = 'User ID not found in data base'
        return new ErrorResponse(HttpStatus.NOT_FOUND, errorMessage, logMessage)
      }

      user.bookList = user.bookList.filter((bookItem) => bookItem.id !== bookItemId)
      await this.usersService.updateUser(userId, user)
      return new SuccessResponse(HttpStatus.OK, user)
    } catch (err) {
      const errorMessage = 'Problem ocurred while saving in database'
      return new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage, err)
    }
  }

  async addBookAnswers(answersData: BookAnswerHistory) {
    try {
      const bookAnswersModelobj = new this.bookAnswersModel(answersData)
      const res = await bookAnswersModelobj.save()
      return new SuccessResponse(HttpStatus.OK, res)
    } catch (err) {
      const errorMessage = 'Problem ocurred while saving in database'
      return new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage, err)
    }
  }
}
