import { Controller, Patch, Param, Body, Res, UseGuards, Post } from '@nestjs/common'
import { Response } from 'express'
import { BookService } from './book.service'
import { UsersService } from 'src/users/users.service'
import { BookItem } from 'src/schemas/bookItem.schema'
import { BookAnswerHistory } from 'src/schemas/bookAnswersHistory'
import { AuthGuard } from '@nestjs/passport'

@UseGuards(AuthGuard('jwt'))
@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService, private readonly usersService: UsersService) {}

  @Patch('add/:id')
  async addBookItem(@Param('id') userId: number, @Body() bookData: BookItem, @Res() res: Response) {
    const response = await this.bookService.addBookItem(userId, bookData)
    res.status(response.getStatus()).send(response.getData())
  }

  @Patch('modify/:id')
  async modifyBookItem(@Param('id') userId: number, @Body() bookData: BookItem, @Res() res: Response) {
    const response = await this.bookService.modifyBookItem(userId, bookData)
    res.status(response.getStatus()).send(response.getData())
  }

  @Patch('remove/:userId/:bookItemId')
  async removeBookItem(@Param('userId') userId: number, @Param('bookItemId') bookItemId: string, @Res() res: Response) {
    const response = await this.bookService.removeBookItem(userId, bookItemId)
    res.status(response.getStatus()).send(response.getData())
  }

  @Post('')
  async addBookAnswers(@Body() answersData: BookAnswerHistory, @Res() res: Response) {
    const response = await this.bookService.addBookAnswers(answersData)
    res.status(response.getStatus()).send(response.getData())
  }
}
