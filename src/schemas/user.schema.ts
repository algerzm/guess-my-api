import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import { BookItem } from './bookItem.schema'

export type UserDocument = User & Document

@Schema()
export class User {
  constructor(email?: string, userName?: string, password?: string) {
    this.email = email
    this.userName = userName
    this.password = password
  }

  @Prop({ required: true })
  email: string

  @Prop({ required: true })
  userName: string

  @Prop({ required: true })
  password: string

  @Prop({ default: 0 })
  compatibility: Number

  @Prop({ default: 0 })
  answeredBooks: Number

  @Prop({ required: true, default: new Date() })
  creationDate: Date

  @Prop({ required: true, default: 'http://navizm.com/gm/images/profile/default.png' })
  imageUrl: string

  @Prop({ default: [] })
  bookList: Array<BookItem>
}

export const UserSchema = SchemaFactory.createForClass(User)
