import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type BookAnswerHistoryDocument = BookAnswerHistory & Document

@Schema()
export class BookAnswerHistory {
  @Prop({ required: true })
  ownersBookId: number

  @Prop({ required: true })
  userThatAnswersId: number

  @Prop({ required: true })
  numberOfQuestions: number

  @Prop({ required: true })
  numberOfCorrectAnswers: number
}

export const BookAnswerHistorySchema = SchemaFactory.createForClass(BookAnswerHistory)
