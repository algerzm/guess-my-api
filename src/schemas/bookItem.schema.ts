import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type BookItemDocument = BookItem & Document

@Schema()
export class BookItem {
  constructor(correctOptionText: string, wrongOptionText: string, wrongOptionImage: string = '', correctOptionImage: string = '') {
    this.correctOptionText = correctOptionText
    this.correctOptionImage = correctOptionImage
    this.wrongOptionText = wrongOptionText
    this.wrongOptionImage = wrongOptionImage
  }

  @Prop({ required: true })
  correctOptionText: string

  @Prop({ required: true, default: '' })
  correctOptionImage: string

  @Prop({ required: true })
  wrongOptionText: string

  @Prop({ required: true, default: '' })
  wrongOptionImage: string

  @Prop({ required: true })
  id: string
}

export const BookItemSchema = SchemaFactory.createForClass(BookItem)
