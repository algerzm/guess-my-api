import { HttpStatus, Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { User, UserDocument } from '../schemas/user.schema'
import { InjectModel } from '@nestjs/mongoose'
import { ErrorResponse } from 'src/classes/common/ErrorResponse.class'
import { SuccessResponse } from 'src/classes/common/SuccessResponse.class'
import { HttpResponse } from 'src/classes/common/HttpResponse.class'
import { Validator } from 'src/utils/validator'
import * as bcrypt from 'bcrypt'
import { signupData } from 'src/interfaces/users/signupData.interface'

const validate = new Validator()
const rounds = 10
@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {}

  async getAllUsers() {
    let response: HttpResponse
    try {
      const users = await this.userModel.find()
      response = new SuccessResponse(HttpStatus.OK, [...users])
    } catch (err) {
      let message = 'Unable to get users from database'
      response = new ErrorResponse(HttpStatus.SERVICE_UNAVAILABLE, message, err)
    }
    return response
  }

  async getUserById(id: number) {
    let response: HttpResponse
    let user = null
    try {
      user = await this.userModel.findOne({ _id: id })
      if (user) {
        response = new SuccessResponse(HttpStatus.OK, user)
      } else {
        let message = 'User not found.'
        let logMessage = 'User id not found in database.'
        response = new ErrorResponse(HttpStatus.NOT_FOUND, message, logMessage)
      }
    } catch (err) {
      let message = 'Error: User not found.'
      let logMessage = 'Something went wron while consulting user in database.'
      response = new ErrorResponse(HttpStatus.BAD_REQUEST, message, logMessage)
    }
    return { response, user }
  }

  async getUserByUserName(userName: string) {
    let response: SuccessResponse | ErrorResponse
    try {
      let user = await this.userModel.findOne({ userName: { $regex: new RegExp(userName, 'i') } })
      if (user) {
        response = new SuccessResponse(HttpStatus.OK, user)
      } else {
        let message = 'User not found.'
        let logMessage = 'User id not found in database.'
        response = new ErrorResponse(HttpStatus.NOT_FOUND, message, logMessage)
      }
    } catch (err) {
      let message = 'Error: User not found.'
      let logMessage = 'Something went wron while consulting user in database.'
      response = new ErrorResponse(HttpStatus.BAD_REQUEST, message, logMessage)
    }

    return response
  }

  async getUserByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email: email }).lean()
  }

  async createUser(user: signupData) {
    let { email, userName, password } = user
    const { emailExists, userName: userNameExists } = validate.validatorMessages
    const { error, message } = validate.chooseErrorMessage(email, password)

    if (error) {
      return new ErrorResponse(HttpStatus.BAD_REQUEST, message, message)
    }

    let userByEmail = await this.getUserByEmail(email)
    if (userByEmail) {
      return new ErrorResponse(HttpStatus.CONFLICT, emailExists, emailExists)
    }

    let userByUserName = await this.getUserByUserName(userName)
    if (userByUserName) {
      return new ErrorResponse(HttpStatus.CONFLICT, userNameExists, userNameExists)
    }

    password = await bcrypt.hash(password, rounds)
    let userWithNewPassword = new User(email, userName, password)

    const newUser = new this.userModel(userWithNewPassword)
    const res = await newUser.save()
    return new SuccessResponse(HttpStatus.OK, res)
  }

  async updateUser(id: number, user: User) {
    await this.userModel.findByIdAndUpdate(id, user).lean()
    return user
  }
}
