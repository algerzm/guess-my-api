import { Controller, Get, Post, Param, Body, Res, UseGuards } from '@nestjs/common'
import { Response } from 'express'
import { UsersService } from './users.service'
import { signupData } from 'src/interfaces/users/signupData.interface'
import { AuthGuard } from '@nestjs/passport'

@UseGuards(AuthGuard('jwt'))
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async getAllUser(@Res() res: Response) {
    const response = await this.usersService.getAllUsers()
    res.status(response.getStatus()).send(response)
  }

  @Get(':id')
  async getUser(@Param('id') id: number, @Res() res: Response) {
    const { response } = await this.usersService.getUserById(id)
    res.status(response.getStatus()).send(response)
  }

  @Get('name/:name')
  async getUserByUserName(@Param('name') name: string, @Res() res: Response) {
    const response = await this.usersService.getUserByUserName(name)
    res.status(response.getStatus()).send(response.getData())
  }

  @Post('signup')
  async createUser(@Body() user: signupData, @Res() res: Response) {
    const response = await this.usersService.createUser(user)
    res.status(response.getStatus()).send(response.getData())
  }
}
